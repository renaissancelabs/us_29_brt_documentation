Coding Assumptions
==================

The analysis of Project 21-23 required developing General Transit Feed Specification 
(GTFS) files to model the proposed BRT service in addition to the planned Flash 29 BRT route.

The US 29 BRT would carry passengers along a 23-mile route between Ellicott City, Maryland, 
in the north and the Silver Spring Transit Center in the south. The Flash 29 BRT will operate 
a route similar to the US 29 BRT service between Burtonsville and the Silver Springs Transit 
Center while providing service to additional stops.

The 29 BRT project would make a total of 11 stops at the following locations: US 40 and Normandy
Center Drive, its northern terminus; MD 103 and Long Gate Parkway; Symphony Woods Road and Mall 
Ring Road; Johns Hopkins Road and APL Drive; MD 216 and Old Columbia Road; US 29 and National Drive;
Briggs Chaney Road and Gateshead Manor Way; US 29 and Tech Road; US 29 and Burnt Mills Avenue; US 29 
and MD 193; and Silver Spring Metro (MD 384 and MD 410), its southern terminus. At the Silver Springs 
Transit Center, riders will be able to connect to the WMATA Red Line and MARC trains.
 
Stops would vary depending on the direction of transit vehicles and time of day, as detailed in the 
next section. The 29 BRT would provide service to five stops the Flash 29 is also planned to serve. 
These stops are at Burtonsville, US 29 and Tech Road, US 29 and Burnt Mills Avenue, US 29 and Maryland 
193, and Silver Spring. Vehicles would operate on 10-minute headways Monday through Friday from 5:30 a.m. 
to 7:30 p.m. They would use dedicated ROW on the shoulder of US 29 north of Tech Road and in mixed 
traffic south of Tech Road. 

**The 29 BRT service is expected to have a 64-minute travel time. The companion Flash BRT is expected to 
have a 31-minute travel time. A 1-minute transfer was coded into the service schedule, allowing for 
coordinated transfers between BRT services at Burtonsville. Service on both BRT segments is expected 
to operate Monday through Sunday. On Monday-Thursday, service is expected to operate from 5:00 AM to 
midnight. On Friday and Saturday, service is expected to operate from 5:00 AM to 2:00 AM the following 
morning. Headways are expected to be 7.5 minutes for peak periods, 15 minutes of off-peak periods, and 
30 minutes for late night and weekend service.**

1. Alignment
------------

Service on the 29 BRT corridor would operate in mixed traffic and dedicated right of way (ROW) on the inside 
road shoulder. The route operates primarily along the US 29 corridor, except for portions where vehicles would 
exit to access a transit stop off the corridor. The service is designed around peak and off-peak periods, with 
stops served varying with the time of day and direction (north or south) of transit vehicles.

In the morning, US 29 vehicles heading south toward the Silver Spring Transit Center would be heading in the peak 
direction, with northbound vehicles heading in the off-peak direction. In the evening, vehicles headed north toward 
Ellicott City, ending at US 40 and Normandy Center Drive, would be heading in the peak direction, with southbound 
vehicles heading in the off-peak direction.

The following describes the route alignment in the southbound peak direction, and notes deviations in service with 
the off-peak direction:

* Southbound US 29 BRT service would begin at US 40 (Baltimore National Pike) and Normandy Center Drive in Ellicott City. 
  Vehicles would begin the route south by entering US 29 by using ROW on the road shoulder. 


* Once on US 29, vehicles would continue south, exiting the highway at the MD-103 East (St. Johns Lane) exit to serve a
  stop at the Long Gate shopping center (MD 103 and Long Gate Parkway).


* The route would then serve the Columbia Town Center, reaching the stop from US 29 via the Little Patuxent Parkway. 
  The route would serve this stop only in the peak direction. In the off-peak direction, the it would serve the John Hopkins University 
  Applied Physics Lab at Johns Hopkins Road and Applied Physics Lab Drive.


* Service would continue south on US 29, exiting at Scaggsville Road (MD 216) to serve a stop at the Scaggsville Park and Ride.


* From the Scaggsville Park and Ride, the route would continue south on US 29 to serve the Burtonsville Park and Ride via the National Drive.


* From there, the route would continue south on US 29 to serve a stop at the Burtonsville Park and Ride. 


* Service would continue south on US 29, existing at Briggs Chaney Road to serve the Briggs Chaney Park and Ride.


* The route will continue south on US 29, where the alignment would shift to mix traffic after serving a stop at Tech Road, ending at the 
  Silver Spring Transit Center.

**The complementary Northbound Flash BRT service would also begin at Silver Spring Transit Center, 
with transit vehicles traveling north along US 29. Stops would occur at Fenton Street, University 
Boulevard, Burnt Mills, Tech Road along US 29, with service terminating at the Burtonsville Crossing Mall.**

The details of how the US 29 BRT and Flash BRT were represented in GTFS are provided below by describing four key tables 
in the feed: Stops.txt, Trips.txt, Stop_times.txt, and Frequencies.txt. Other tables in the new Project 20-19 feed are based 
on standard tables defined in the Chapter 30 Transit Accessibility Scoring Guide (“Standard GTFS Tables” section) and 
are not described here.


2. Attributes
-------------

The attributes of the base multimodal network were modified through the addition of the newly developed GTFS feed 
modeling Project 21-23. The details of how Project 21-23 was represented in GTFS are provided below by describing 
four key tables of the feeds: Stops.txt, Trips.txt, Stop_times.txt, and Frequencies.txt. 

Other tables in the new project feed are based on standard tables defined in the Chapter 30 Transit Accessibility 
Scoring Guide (“Standard GTFS Tables” section) and are not described here.


i. Stops.txt
~~~~~~~~~~~~

**Table 1: US 29 BRT Stops Table**

+---------+-----------------------------------+--------------+-------------+
| stop_id | stop_name                         | stop_lon     | stop_lat    |
+---------+-----------------------------------+--------------+-------------+
| 1       | Silver Spring Transit Center      | -77.03094996 | 38.99422862 |
+---------+-----------------------------------+--------------+-------------+
| 3N      | University Boulivard - Northbound | -77.01414711 | 39.01864378 |
+---------+-----------------------------------+--------------+-------------+
| 3S      | University Boulivard - Southbound | -77.01264085 | 39.02065048 |
+---------+-----------------------------------+--------------+-------------+
| 4N      | Burnt Mills - Northbound          | -77.00440949 | 39.03135898 |
+---------+-----------------------------------+--------------+-------------+
| 4S      | Burnt Mills - Southbound          | -77.00456485 | 39.03166715 |
+---------+-----------------------------------+--------------+-------------+
| 5N      | Tech Road - Northbound            | -76.96799729 | 39.05799068 |
+---------+-----------------------------------+--------------+-------------+
| 5S      | Tech Road - Southbound            | -76.96833953 | 39.05831968 |
+---------+-----------------------------------+--------------+-------------+
| 6       | Burtonsville                      | -76.930596   | 39.1140225  |
+---------+-----------------------------------+--------------+-------------+
| 7N      | Applied Physics Lab               | -76.8994066  | 39.1607909  |
+---------+-----------------------------------+--------------+-------------+
| 7S      | Scaggsville                       | -76.9086757  | 39.1468464  |
+---------+-----------------------------------+--------------+-------------+
| 8       | Columbia Town Center              | -76.8600467  | 39.2124986  |
+---------+-----------------------------------+--------------+-------------+
| 9       | Long Gate                         | -76.8180099  | 39.2527972  |
+---------+-----------------------------------+--------------+-------------+
| 10      | US 40 West                        | -76.8062     | 39.2856     |
+---------+-----------------------------------+--------------+-------------+


ii. Trips.txt (vehicle-trip enumeration)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The trips file represents a single route with a seven-day travel profile. The file further splits the 
route into trips, each identified by a specific ID, that reflect the various frequencies of the line. 
The US 29 BRT is represented as two routes, one for northbound trips and another for southbound trips. 
These different routes represent the differing stop sequences for transit vehicles depending on their 
direction of travel.

**Table 2: Project 20-34 Trips Table**

+----------+------------+-------------+
| route_id | service_id | trip_id     |
+----------+------------+-------------+
| US29BRT  | MTWTF      | us29_brt_NB |
+----------+------------+-------------+
| US29BRT  | MTWTF      | us29_brt_SB |
+----------+------------+-------------+


iii.	Stop_times.txt (transit schedule)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The stop times table was developed by determining an average travel speed based on the travel time and 
approximate length of the line.

**Table 3: US 29 BRT Stops Times Table**

+-------------+--------------+----------------+---------+---------------+
| trip_id     | arrival_time | departure_time | stop_id | stop_sequence |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 6:01:00      | 6:01:00        | 1       | 1             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 6:10:00      | 6:10:00        | 3N      | 2             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 6:12:32      | 6:12:32        | 4N      | 3             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 6:19:00      | 6:19:00        | 5N      | 4             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 6:32:00      | 6:32:00        | 6       | 5             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 6:49:01      | 6:49:01        | 7N      | 6             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 7:06:43      | 7:06:43        | 8       | 7             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 7:23:08      | 7:23:08        | 9       | 8             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_NB | 7:31:00      | 7:31:00        | 10      | 9             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 6:01:00      | 6:01:00        | 10      | 1             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 6:09:41      | 6:09:41        | 9       | 2             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 6:24:25      | 6:24:25        | 8       | 3             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 6:53:11      | 6:53:11        | 7S      | 4             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 7:04:00      | 7:04:00        | 6       | 5             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 7:17:00      | 7:17:00        | 5S      | 6             |
+-------------+--------------+----------------+---------+---------------+
| us29_brt_SB | 7:35:00      | 7:35:00        | 1       | 9             |
+-------------+--------------+----------------+---------+---------------+


iv.	Frequencies.txt (frequency of recurring trips)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A frequencies table was developed to model the differing headways of the line throughout a
day of typical service. The table defines a start and end time for a headway, with each headway
associated with a trip ID. The headways are provided in seconds. The trip IDs link the headway
times to the individual trips that comprise the route described in the trips.txt file. When
frequencies table is used, the stop times file defines a template of stop sequences and travel
times between stops, and the frequencies table defines the interval of recurrence for each trip
following the stop times template.

**Table 4: US 29 BRT Frequencies Table**

+-------------+----------------+--------------+------------------+
| trip_id     | start_time     | end_time     | headway_secs     |
+-------------+----------------+--------------+------------------+
| us29_brt_NB | 6:00:01        | 10:00:00     | 600              |
+-------------+----------------+--------------+------------------+
| us29_brt_SB | 6:00:01        | 10:00:00     | 600              |
+-------------+----------------+--------------+------------------+



.. toctree::
   :maxdepth: 2
   :caption: Contents: