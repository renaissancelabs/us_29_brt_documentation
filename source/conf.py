# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------

project = 'MDOT Chapter 30 Report: US 29 BRT'
copyright = '2020, Renaissance Planning'
author = 'Renaissance Planning'

# The full version, including alpha/beta/rc tags
release = '2020'

master_doc = 'overview'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.'

sys.path[0:0] = [os.path.abspath('_themes/foundation-sphinx-theme')]
extensions = ['sphinx.ext.autodoc', 'foundation_sphinx_theme']


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

import foundation_sphinx_theme

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'foundation_sphinx_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

html_static_path = ['_static']

html_theme_path = foundation_sphinx_theme.HTML_THEME_PATH

html_title = "MDOT Chapter 30 Report: US 29 BRT"

html_theme_options = {
     #'top_bar_content_title': 'Sections',
     #'collapse_navigation': False
}