Reasonableness of Results
=========================


1. Extent of Study Area
-----------------------

The project study area is based on travel time to the zones in which the project is 
implemented – any zones within 45 minutes by transit (in the project build scenario) 
are part of the study area as are any zones within 15 minutes by driving (based on MSTM 
highway skims).

The study area comprises 341 square miles that includes portions of Montgomery County, 
Howard County, Washington, DC, and Ellicott City in Maryland. It extends approximately 
2 miles north of the northern terminus of US 29 BRT corridor at US 40 and Normandy Center 
Drive and extends approximately 8 miles south of the southern terminus of the corridor, 
the Silver Spring Metro stop.


2. Travel Time Contours to Project
----------------------------------

The service area analysis shows that the project brings more of the study area within 
a one-hour travel time by transit. With the introduction of the US 29 BRT, service from 
the northern terminus of the project extends farther south, growing to encompass a residential 
area of Red Hill Branch southeast of US 29 and MD 100. One-hour service would also extend 
farther south of Columbia Town Center to a proposed stop at the John Hopkins University 
Applied Physics Lab, about two miles north of the Patuxent River.


3. MMA Results
--------------

**Base Accessibility**

.. raw:: html

    <iframe src="_static\map_html\base_accessibility.html" height="525" width="100%" scrolling="no" style="border:none;" seamless="seamless"></iframe>


**Build Accessibility**

.. raw:: html

    <iframe src="_static\map_html\build_accessibility.html" height="525" width="100%" scrolling="no" style="border:none;" seamless="seamless"></iframe>