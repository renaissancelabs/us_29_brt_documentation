Network Review Results
======================

1. Network Dataset Configuration
--------------------------------

The multi-modal network is composed of five features classes:

* “TransitLines” represents vehicle trips between transit stops;

* “Stops” represents transit stops served by transit vehicles;

* “Streets_UseThisOne” represents the local street network for access to/egress from transit stops;

* “Stops_Snapped2Street” represents each transit stop as a location “snapped” to the “Streets_UseThisOne” features;

* “Connectors_Stops2Street” represents the connections from the L3_HWY features to the transit Stop locations via the Stops_Snapped2Street junctions. 

These features are grouped into three “connectivity groups” that enforce appropriate routing for transit network problems. Modeled paths must begin on the streets (“Streets_UseThisOne”) network (column 1) and can proceed to the transit lines (column 3) via the Connectors_Stops2Streets features (column 2). The network edge features are connected through the Stops (column 2-column 3) and the Stops_Snapped2Streets (column 1-column 2) node features. Figure 4 below shows the connectivity topological rules applied to the development of the US 29 BRT multimodal network.

Travel times between zones are computed using a travel time evaluator, setup for the US 29 BRT multimodal network in Network Analyst as shown in Figure X. A 3 mph (264 feet per minute) walk speed is assumed and the transit evaluator computes time-of-departure-specific transit travel times using the GTFS schedule.

2. Connectivity Tests
---------------------

There are several steps required to create a multi-modal network, presenting opportunities for user error or technical anomalies to mis-represent network connectivity. To test the connectivity and usability of the US 29 BRT multimodal network, two quality control tests were performed: Service area analysis and routing analysis. These tests are described below. All test cases are based on an assumed departure time of 8:00 AM on a typical Wednesday. 

Service Area Test
~~~~~~~~~~~~~~~~~

Figures 2 and 3 show the result of the service area analysis. The maps visualize travel time isochrones for a single location within the project study area for the base network and the build network with the 29 BRT service. A simple check that the isochrones expand as the project provides additional service and that the expansion is intuitive given the nature of the project is sufficient to confirm that the project is appropriately integrated into the base multimodal network.

A comparison between the service areas shows a clear expansion of service relative to the location used in the test, the Columbia Town Center, when US 29 BRT service is introduced. Areas within 60 minutes of the Columbia Town Center expand. In particular, travel times to a proposed stop at the John Hopkins University Applied Physics Lab fall within an hour.

These changes confirm that US 29 BRT is appropriately integrated into the base multimodal network.


*Figure 2: Base Service Area*

.. raw:: html

    <iframe src="_static\map_html\base_service_area.html" height="525" width="100%" scrolling="no" style="border:none;" seamless="seamless"></iframe>


*Figure 3: Build Service Area*

.. raw:: html

    <iframe src="_static\map_html\build_service_area.html" height="525" width="100%" scrolling="no" style="border:none;" seamless="seamless"></iframe>


Route Test
~~~~~~~~~~

Figures 4 compares shortest path routing between two points. For these tests, beginning and ending points were identified near the termini of the US 29 BRT system. 

The test confirms the network is utilizing the new path the service creates. In the no-build network, the shortest path between Ellicott City south to Silver Spring is a circuitous path to the east of US 29. In the build network, the shortest path is a more direct route between these points made available by the US 29 BRT.

The test also shows a travel time reduction after the addition of US 29 BRT. The no-build shortest route takes approximately 3 hours, 22 minutes, to traverse, while the build route takes 1 hour, 46 minutes. 

This test further confirmed US 29 BRT was appropriately integrated into the base multimodal network.

*Figure 4: Route test comparison*

.. raw:: html

    <iframe src="_static\map_html\route_test.html" height="525" width="100%" scrolling="no" style="border:none;" seamless="seamless"></iframe>
