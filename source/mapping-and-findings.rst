Mapping and Findings
--------------------

1.	Network

[ Project Links and Stops ]


2.	Build vs. No Build

[ Average travel time changes  ]


3.	TAZ

The MMA results show an increase in the number of jobs accessible by transit when compared to the no build option. 
In the 29 BRT only scenario, portions of the study area see an increase in job accessibility ranging from XX to more than XX. 

[ Positive and negative accessibility changes ]


4.	Project findings

As shown in Figure X, the largest increases in job access occur in the TAZs in closest proximity to US 29 BRT’s alignment. 
The addition of project increased the number of accessible jobs for the general population by XX percent. Accessibility 
increases for the disadvantaged population are XX percent and less than XX percent, respectively. The following table summarizes 
the change in job accessibility for the general and disadvantaged populations in the study area.

[ TABLE OF FINDINGS ]

Access to Jobs in Base (general population)
Access to Jobs in Build (general population) 
Change in Access to Jobs (general population) 
Access to Jobs in Base (disadvantaged population) 
Access to Jobs in Build (disadvantaged population) 
Change in Access to Jobs (disadvantaged population) 
Change in Average Travel Time Per Trip 
Estimated Ridership  
Estimated Existing Ridership (Estimated Ridership * 80%) 
Total Estimated Travel Time Savings for Existing Transit Riders (Change in Average Travel Time Per Trip * Estimated Existing Ridership) 



.. toctree::
   :maxdepth: 2
   :caption: Contents:
