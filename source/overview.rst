US 29 BRT Project Report
========================

Overview of Project
-------------------

Project 21-23 would add Bus Rapid Transit (BRT) service between stations in 
Ellicott City, the county seat of Howard County, and Silver Spring in Montgomery 
County, with nine intermediate stops. The service would provide connections to 
the Flash BRT, which scheduled to open on US 29 between Silver Spring and 
Burtonsville in 2020.

**Figure 1: Proposed US 29 BRT Service**

.. raw:: html
      
      <iframe src="_static\map_html\route.html" height="525" width="100%" scrolling="no" style="border:none;" seamless="seamless"></iframe>

.. toctree::
   :maxdepth: 2

   introduction
   coding-assumptions
   network-review-results
   reasonableness-of-results
   mapping-and-findings